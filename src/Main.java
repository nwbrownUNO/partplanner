

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main viewing class
 */
@WebServlet("/")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set the response MIME type
		response.setContentType("text/html");
		
		PrintWriter web = response.getWriter();
		web.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"../assets/style.css\"><title>Part Planner</title></head><body>");
	
		//Nav Bar
		web.println("<ul><li>"
				+ "<a href=\"./\">Home</a></li>"
				+ "<li><a href=\"./add\">Add</a></li>"
				+ "<li><a href=\"./del\">Remove</a></li>"
				+ "<li><a href=\"./search\">Search</a></li>"
				+ "</ul>");
		
		//Break
		web.println("<br/>"); 
		
		
		//Table Top 
		web.println("<table>"); 
		web.println("<tr><th>Part Name</th><th>Part Number</th><th>Height</th><th>Width</th><th>Length</th><th>Description</th></tr>"); //Java needs multiline stirng support
		
		//Data
		String glob = SQLFactory.getParts(); 
		web.println("<!--\n"+glob+"\n-->"); 
		
		String[] parts = glob.split("\n");
		for(String part : parts){
			String[] data = part.split(","); 
			web.println("<tr>"); 
			web.println("<td>"+data[0]+"</td>");
			web.println("<td>"+data[1]+"</td>");
			web.println("<td>"+data[2]+"</td>");
			web.println("<td>"+data[3]+"</td>");
			web.println("<td>"+data[4]+"</td>");
			web.println("<td>"+data[5]+"</td>");
			web.println("</tr>"); 
		}
		
		//table botton
		web.println("</table>"); 
		
		web.println("</body></html>"); 
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
