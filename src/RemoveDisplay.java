

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main viewing class
 */
@WebServlet("/del")
public class RemoveDisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set the response MIME type
		response.setContentType("text/html");
		
		PrintWriter web = response.getWriter();
		web.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"../assets/style.css\"><title>Part Planner</title></head><body>");
		
		//Nav Bar
		web.println("<ul><li>"
				+ "<a href=\"./\">Home</a></li>"
				+ "<li><a href=\"./add\">Add</a></li>"
				+ "<li><a href=\"./del\">Remove</a></li>"
				+ "<li><a href=\"./search\">Search</a></li>"
				+ "</ul>");
		
		//Break
		web.println("<br/>"); 
		
		//Start content 
		//Start content
		web.println("<form action=\"./del\" method=\"post\">");
		web.println("Enter Part Number/Number to remove:");
		web.println("<input type=\"text\" name=\"num\">");
		web.println("<input type=\"submit\" value=\"Remove\">");
		web.println("</form> ");
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("num") == null){
			response.sendRedirect("./error?id=3");
			return; 
		}
		
		int r = SQLFactory.removePart(request.getParameter("num")); 
		
		if(r == 0){
			response.sendRedirect("./success?id=1");
			return; 
		} else {
			response.sendRedirect("./error?id=5");
			return; 
		}
		
	}

}
