

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main viewing class
 */
@WebServlet("/add")
public class AddDisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set the response MIME type
		response.setContentType("text/html");
		
		PrintWriter web = response.getWriter();
		web.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"../assets/style.css\"><title>Part Planner</title></head><body>");
		
		//Nav Bar
		web.println("<ul><li>"
				+ "<a href=\"./\">Home</a></li>"
				+ "<li><a href=\"./add\">Add</a></li>"
				+ "<li><a href=\"./del\">Remove</a></li>"
				+ "<li><a href=\"./search\">Search</a></li>"
				+ "</ul>");
		
		//Break
		web.println("<br/>"); 
		
		//Start content
		web.println("<div>"); 
		web.println("<form action=\"./add\" method=\"post\">"); 
		
		web.println("<label for=\"pname\">Part Name</label>"); 
		web.println("<input type=\"text\" id=\"pname\" name=\"pname\" placeholder=\"Part Name\">"); 
		
		web.println("<label for=\"num\">Part Number</label>"); 
		web.println("<input type=\"number\" id=\"num\" name=\"num\" placeholder=\"Part Number\">"); 

		web.println("<label for=\"height\">Height</label>"); 
		web.println("<input type=\"number\" id=\"height\" name=\"height\" placeholder=\"Height\">"); 
		    
		web.println("<label for=\"width\">Width</label>"); 
		web.println("<input type=\"number\" id=\"width\" name=\"width\" placeholder=\"Width\">"); 
		    
		web.println("<label for=\"length\">Length</label>"); 
		web.println("<input type=\"number\" id=\"lenght\" name=\"length\" placeholder=\"Length\">"); 
		    
		web.println("<label for=\"descr\">Description</label>"); 
		web.println("<input type=\"text\" id=\"descr\" name=\"descr\" placeholder=\"Brief description\">"); 

		web.println("<input type=\"submit\" value=\"Submit\">"); 
		web.println("</form>"); 
		web.println("</div>");
		
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.getParameter("num"
		if(request.getParameter("pname") != null &&
				request.getParameter("num") != null &&
				request.getParameter("height") != null &&
				request.getParameter("width")!= null &&
				request.getParameter("length")!= null &&
				request.getParameter("descr")!= null){
			
			//Validate Descr 
	
			String descr = request.getParameter("descr");
			if(descr.length() > 200){
				descr = descr.substring(0,200); 
			}
			descr = descr.split("'")[0]; //Make sure it's not in there 
			
			String name = request.getParameter("pname");
			if(name.length() > 200){
				name = name.substring(0,200); 
			}
			name = name.split("'")[0]; //Make sure it's not in there 
			
			int num = 0; 
			int height = 0; 
			int width = 0;  
			int length = 0; 
			
			try{
				num = Integer.parseInt(request.getParameter("num")); 
				height = Integer.parseInt(request.getParameter("height")); 
				width = Integer.parseInt(request.getParameter("width")); 
				length = Integer.parseInt(request.getParameter("length")); 
			} catch(Exception e){
				response.sendRedirect("./error?id=0");
				return; 
			}
			
			int id = SQLFactory.addPart(name, num, height, width, length, descr); 
			if(id == 0){
				response.sendRedirect("./success?id=0"); 
				return; 
			} else {
				response.sendRedirect("./error?id=2"); 
				return; 
			}
		} else {
			response.sendRedirect("./error?id=1");
			return; 
		}
	}

}
