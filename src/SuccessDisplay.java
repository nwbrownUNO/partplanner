

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main viewing class
 */
@WebServlet("/success")
public class SuccessDisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SuccessDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set the response MIME type
		response.setContentType("text/html");
		
		PrintWriter web = response.getWriter();
		web.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"../assets/style.css\"><title>Part Planner</title></head><body>");
		
		//Nav Bar
		web.println("<ul><li>"
				+ "<a href=\"./\">Home</a></li>"
				+ "<li><a href=\"./add\">Add</a></li>"
				+ "<li><a href=\"./del\">Remove</a></li>"
				+ "<li><a href=\"./search\">Search</a></li>"
				+ "</ul>");
		
		//Break
		web.println("<br/>"); 
		
		//Start content  
		if(request.getParameter("id") == null){
			web.println("<h1>Unknown error occured, this is not successful!"); 
		}
		
		int id = Integer.parseInt(request.getParameter("id")); 
		
		if(id == 0){
			web.println("<h1>Part Successfully added!</h1>");  
		}
		
		if(id == 1){
			web.println("<h1>Parts matching that number/name have been removed!</h1>"); 
		}
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
