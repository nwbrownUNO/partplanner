import java.sql.*;
import java.util.Properties;

public class SQLFactory {
	private static final String dbClassName = "com.mysql.jdbc.Driver";
	private static final String CONNECTION = "jdbc:mysql://127.0.0.1/data";

	private static Connection getConnection(){
		try {
			Class.forName(dbClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	    Properties p = new Properties();
	    p.put("user","data");
	    p.put("password","password");
	    
	    Connection c;
		try {
			c = DriverManager.getConnection(CONNECTION,p);
			return c; 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null; 
	}
	
	
	public static String getParts(){
		Connection c = getConnection(); 
		String query = "SELECT * FROM parts"; 
		String data = ""; 
		try {
			PreparedStatement ps = c.prepareStatement(query);
			ResultSet rs = ps.executeQuery(); 
			
			while(rs.next()){
				data += rs.getString("partName") + ","; 
				data += rs.getInt("partNumber") + ",";
				data += rs.getInt("height") + ","; 
				data += rs.getInt("width") + ","; 
				data += rs.getInt("length") + ","; 
				data += rs.getString("descr") + "\n"; 
			}
			
			c.close(); 
			
			return data; 
			
		} catch (SQLException e) {
			return "("+e.toString()+")ERROR,ERROR,ERROR,ERROR,ERROR,ERROR"; 
		} 			
	}
	
	public static String getParts(String s){
		Connection c = getConnection(); 
		String query = "SELECT * FROM parts WHERE partNumber LIKE '"+s+"' OR partName LIKE '%"+s+"%'"; 
		String data = ""; 
		try {
			PreparedStatement ps = c.prepareStatement(query);
			ResultSet rs = ps.executeQuery(); 
			
			while(rs.next()){
				data += rs.getString("partName") + ",";
				data += rs.getInt("partNumber") + ",";
				data += rs.getInt("height") + ","; 
				data += rs.getInt("width") + ","; 
				data += rs.getInt("length") + ","; 
				data += rs.getString("descr") + "\n"; 
			}
			
			
			if(data.equals("")){
				data = "No match,,,,,,\n"; 
			}
			
			c.close(); 
			
			return data; 
			
		} catch (SQLException e) {
			return "("+e.toString()+")ERROR,ERROR,ERROR,ERROR,ERROR,ERROR"; 
		} 			
	}
	
	public static int removePart(String s){
		Connection c = getConnection(); 
		int i = -1; 
		String query = ""; 
		
		try{
			i = Integer.parseInt(s); 
			query = "DELETE FROM `parts` WHERE partNumber = '"+i+"'"; 
		}catch(Exception e){
			query = "DELETE FROM `parts` WHERE partName = '"+s+"'"; 
		}
		
		try {
			PreparedStatement ps = c.prepareStatement(query);
			ps.executeUpdate(); 
			
			c.close(); 
			
			return 0; 
			
		} catch (SQLException e) {
			return 1; 
		} 			
	}
	
	
	public static int addPart(String name, int id, int height, int width, int length, String descr){
		Connection c = getConnection(); 
		String query = "INSERT INTO `parts` (`partName`, `partNumber`, `height`, `width`, `length`, `descr`) VALUES ('"+name+"', '"+id+"', '"+height+"', '"+width+"', '"+length+"', '"+descr+"')"; 
		String data = ""; 
		try {
			PreparedStatement ps = c.prepareStatement(query);
			ps.executeUpdate(); 
			
			c.close(); 
			
			return 0; 
			
		} catch (SQLException e) {
			return 1; 
		} 			
	}
	

}
